##
## Build Base Image
##
FROM golang:1.18-buster AS build

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY . ./
ENV GOACH=amd64
RUN go build -o /go/bin/app

##
## Build Deploy Image
##
FROM gcr.io/distroless/base-debian11
WORKDIR /
COPY --from=build /go/bin/app /app
EXPOSE 8081

ENTRYPOINT ["/app"]