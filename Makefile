image:
	docker build -t backend:latest -f Dockerfile .

postgres:
	docker run -p 127.0.0.1:5432:5432  --name account_postgres \
	-e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=changeme -e POSTGRES_DB=members \
	-d postgres:latest

run_container:
	docker run -p:8081:8081 --env-file ./test.env --link account_postgres:db --name backend_account backend:latest