package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"

	"example/api-service/account"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

const (
	port = ":8081"
)

var (
	db  *gorm.DB
	err error
)

// Function called for index
func indexView(c *gin.Context) {
	c.Header("Access-Control-Allow-Origin", "*")
	c.Header("Access-Control-Allow-Headers", "access-control-allow-origin, access-control-allow-headers")
	c.JSON(http.StatusOK, gin.H{"message": "MEMBER ACCOUNTS"})
}

func main() {
	fmt.Printf("Welcome to account service! \n")

	// Read Configuration
	err = godotenv.Load("dev.env")
	if err != nil {
		panic(err.Error())
	}
	var DbHost = os.Getenv("DB_HOST")
	var DbPort = os.Getenv("DB_PORT")
	var DbUser = os.Getenv("DB_USER")
	var DbName = os.Getenv("DB_NAME")
	var DbPassword = os.Getenv("DB_PASSWORD")

	// Setup Connect PostgresDB
	// DBURL := "host=localhost user=noppong password=IiEDFQRdbM dbname=member_store port=15432 sslmode=disable TimeZone=Asia/Bangkok"
	DBURL := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Bangkok", DbHost, DbUser, DbPassword, DbName, DbPort)
	db, err = gorm.Open(postgres.Open(DBURL), &gorm.Config{})
	if err != nil {
		fmt.Println(err.Error())
		panic("failed to connect database")
	}

	// Auto migration tables
	if err = db.AutoMigrate(&account.Account{}); err != nil {
		panic(err)
	}

	// Setup Gin Routes
	gin.SetMode(gin.DebugMode)
	router := gin.Default()

	// Set route for index
	router.GET("/", indexView)

	// account end point
	accHandler := account.NewAccountHandler(db)
	router.GET("/accounts", accHandler.ListAccount)
	router.POST("/accounts", accHandler.NewAccount)
	router.GET("/accounts/:id", accHandler.FindAccountById)
	router.DELETE("/accounts/:id", accHandler.DeleteWithId)

	router.Run(port) // listen and serve on 0.0.0.0:8081
}
