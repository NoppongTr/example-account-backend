module example/api-service

go 1.14

require (
	github.com/gin-gonic/gin v1.8.1
	github.com/go-playground/validator/v10 v10.11.0 // indirect
	github.com/joho/godotenv v1.4.0
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/pelletier/go-toml/v2 v2.0.2 // indirect
	golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d // indirect
	golang.org/x/net v0.0.0-20220622184535-263ec571b305 // indirect
	golang.org/x/sys v0.0.0-20220622161953-175b2fd9d664 // indirect
	gorm.io/driver/postgres v1.3.7
	gorm.io/gorm v1.23.6 // indirect
)
