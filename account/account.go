package account

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type Account struct {
	gorm.Model
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Age       int    `json:"age"`
	Phone     string `json:"phone"`
}

func (a *Account) BeforeCreate(db *gorm.DB) error {
	a.Model.ID = uint(time.Now().Unix())
	return nil
}

type AccountHandler struct {
	db *gorm.DB
}

func NewAccountHandler(db *gorm.DB) *AccountHandler {
	return &AccountHandler{db: db}
}

func (a *AccountHandler) NewAccount(c *gin.Context) {
	var account Account

	if err := c.ShouldBindJSON(&account); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	r := a.db.Create(&account)
	if err := r.Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	c.JSON(http.StatusCreated, gin.H{
		"status": "success",
		"data":   account.Model.ID,
	})
}

func (a *AccountHandler) ListAccount(c *gin.Context) {
	var accounts []Account
	// accounts = append(accounts, fackAccounts...)

	r := a.db.Find(&accounts)
	if r.Error != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": r.Error.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"status": "success",
		"data":   accounts,
	})
}

func (a *AccountHandler) FindAccountById(c *gin.Context) {
	var account Account

	idParam := c.Param("id")
	id, err := strconv.Atoi(idParam) // convernt string to int
	fmt.Println("---->", id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}

	result := a.db.Where("ID = ?", uint(id)).Find(&account)
	if result.RowsAffected == 0 {
		c.JSON(http.StatusNotFound, gin.H{
			"status":  false,
			"massage": "no data",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"status":  true,
		"massage": "success",
		"data":    account,
	})
}

func (a *AccountHandler) DeleteWithId(c *gin.Context) {
	var account Account
	id := c.Param("id")

	idParam, err := strconv.Atoi(id)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	if result := a.db.First(&account, uint(idParam)); result.Error != nil {
		c.AbortWithError(http.StatusNotFound, result.Error)
		return
	}

	a.db.Delete(&account)
	c.JSON(http.StatusOK, gin.H{
		"status": "Deleted " + id,
	})
}
